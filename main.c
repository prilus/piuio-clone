/* Keyboard example for Teensy USB Development Board
* http://www.pjrc.com/teensy/usb_keyboard.html
* Copyright (c) 2008 PJRC.COM, LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include <stdio.h>

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>

#define F_CPU (16000000UL)
#include <util/delay.h>

#define CPU_PRESCALE(n) (CLKPR = 0x80, CLKPR = (n))

#include "usb.h"
#include "uart.h"

#define BIT_SET(x, bit, v) (x |= ((!!v) << bit))
#define BIT_GET(x, bit) (x & (1 << bit))

static FILE mystdout = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);

int main(void)
{
	// set for 16 MHz clock
	CPU_PRESCALE(0);
	
	uart_init(9600);

	stdout = &mystdout;
	
	printf("start\r\n");

	// Configure all port B and port D pins as inputs with pullup resistors.
	// See the "Using I/O Pins" page for details.
	// http://www.pjrc.com/teensy/pins.html
	DDRB = 0xFF;
	DDRC = 0x00;
	DDRD = 0xCC;
	DDRF = 0xFF;
	PORTB = 0x00;
	PORTC = 0xFF;
	PORTD = 0x73; // PD6 -> led
	PORTF = 0x00;

	// Initialize the USB, and then wait for the host to set configuration.
	// If the Teensy is powered without a PC connected to the USB port,
	// this will wait forever.
	usb_init();
	while (!usb_configured()) /* wait */ ;

	// Wait an extra second for the PC's operating system to load drivers
	// and do whatever it does to actually be ready for input
	_delay_ms(1000);

	//int t = 0;

	while (1) {
		const uint8_t pc = PINC, pd = PIND;
		
		{
			// led
			uint8_t pb = 0, pf = 0;
			
			// 1p lb, lt, c, rt, rb
			BIT_SET(pb, 3, BIT_GET(dataIn[0], 5));
			BIT_SET(pb, 2, BIT_GET(dataIn[0], 2));
			BIT_SET(pb, 1, BIT_GET(dataIn[0], 4));
			BIT_SET(pb, 0, BIT_GET(dataIn[0], 3));
			BIT_SET(pf, 0, BIT_GET(dataIn[0], 6));
			
			// 2p lb, lt, c, rt, rb
			BIT_SET(pf, 1, BIT_GET(dataIn[2], 5));
			BIT_SET(pf, 2, BIT_GET(dataIn[2], 2));
			BIT_SET(pf, 3, BIT_GET(dataIn[2], 4));
			BIT_SET(pf, 4, BIT_GET(dataIn[2], 3));
			BIT_SET(pf, 5, BIT_GET(dataIn[2], 6));
			
			// svc, test
			BIT_SET(pf, 6, !BIT_GET(pc, 6));
			BIT_SET(pf, 7, !BIT_GET(pc, 7));
			
			PORTB = pb;
			PORTF = pf;
		}
		
		{
			// key
			uint8_t _1p = 0, _2p = 0, svc = 0;
			
			// 1p lb, lt, c, rt, rb
			BIT_SET(_1p, 3, !BIT_GET(pd, 0));
			BIT_SET(_1p, 0, !BIT_GET(pd, 1));
			BIT_SET(_1p, 2, !BIT_GET(pd, 4));
			BIT_SET(_1p, 1, !BIT_GET(pd, 5));
			BIT_SET(_1p, 4, !BIT_GET(pc, 0));
			
			// 2p lb, lt, c, rt, rb
			BIT_SET(_2p, 3, !BIT_GET(pc, 1));
			BIT_SET(_2p, 0, !BIT_GET(pc, 2));
			BIT_SET(_2p, 2, !BIT_GET(pc, 3));
			BIT_SET(_2p, 1, !BIT_GET(pc, 4));
			BIT_SET(_2p, 4, !BIT_GET(pc, 5));
			
			// svc, test
			BIT_SET(svc, 6, !BIT_GET(pc, 6));
			BIT_SET(svc, 1, !BIT_GET(pc, 7));
			
			dataOut[0] = _1p;
			dataOut[2] = _2p;
			dataOut[1] = svc;
		}
		
		/*
		t = ++t % 1000;
		if (!t) {
			printf("test input %x %x\r\n", pc, pd);
			printf("test input2 %x %x %x %x %x %x %x %x\r\n", dataIn[0], dataIn[1], dataIn[2], dataIn[3], dataIn[4], dataIn[5], dataIn[6], dataIn[7]);
		}
		*/
		
		_delay_ms(1);
		
		if(debugmem[0]) {
			printf("%s", debugmem);
			debugmem[0] = 0;
		}
	}
}




